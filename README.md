
## bootc workflow test

### What's bootc workflow test

The bootc workflow test includes [`bootc install`](https://github.com/containers/bootc) test, [`anaconda installation`](https://pykickstart.readthedocs.io/en/latest/kickstart-docs.html#ostreecontainer) test and [`bootc-image-builder`](https://github.com/osbuild/bootc-image-builder) test

* The `bootc install` test will use bootc install command inside bootc container image to install bootc OCI Image
* The anaconda test will install the bootc OCI image from kickstart command ostreecontainer
* The bootc-image-builder test will build AMI, qcow2, raw, vmdk, and anaconda-iso with bootc-image-builder and deploy image

### bootc workflow test chart

![bootc workflow test chart](./image_mode_test_chart.jpg)

### How to run bootc workflow test

The bootc workflow test is running on [Testing Farm](https://docs.testing-farm.io/Testing%20Farm/0.1/index.html).

If you'd like to run bootc workflow test locally, you have to [onboard Testing Farm](https://docs.testing-farm.io/Testing%20Farm/0.1/onboarding.html) first. After you have your own `Testing Farm API Token`, you can run with the floowing command line:


```shell
    testing-farm request \
        --plan $TMT_PLAN \
        --secret TIER1_IMAGE_URL=$TIER1_IMAGE_URL \
        --environment ARCH=$ARCH \
        --secret QUAY_USERNAME=$QUAY_USERNAME \
        --secret QUAY_PASSWORD=$QUAY_PASSWORD \
        --secret QUAY_SECRET=$QUAY_SECRET \
        --secret REGISTRY_STAGE_USERNAME=$REGISTRY_STAGE_USERNAME \
        --secret REGISTRY_STAGE_PASSWORD=$REGISTRY_STAGE_PASSWORD \
        --secret DOWNLOAD_NODE=$DOWNLOAD_NODE \
        --secret RHC_AK=$RHC_AK \
        --secret RHC_ORGID=$RHC_ORGID \
        --secret AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
        --secret AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
        --environment AWS_REGION=$AWS_REGION \
        --secret AZURE_CLIENT_ID=$AZURE_CLIENT_ID \
        --secret AZURE_SECRET=$AZURE_SECRET \
        --secret AZURE_TENANT=$AZURE_TENANT \
        --secret BEAKER_CLIENT_B64=$BEAKER_CLIENT_B64 \
        --secret BEAKER_KEYTAB_B64=$BEAKER_KEYTAB_B64 \
        --secret KRB5_CONF_B64=$KRB5_CONF_B64 \
        --environment GCP_PROJECT=$GCP_PROJECT \
        --secret GCP_SERVICE_ACCOUNT_NAME=$GCP_SERVICE_ACCOUNT_NAME \
        --secret GCP_SERVICE_ACCOUNT_FILE_B64=$GCP_SERVICE_ACCOUNT_FILE_B64 \
        --secret GOVC_URL=$GOVC_URL \
        --secret GOVC_USERNAME=$GOVC_USERNAME \
        --secret GOVC_PASSWORD=$GOVC_PASSWORD \
        --environment GOVC_INSECURE=$GOVC_INSECURE \
        --secret OS_AUTH_URL=$OS_AUTH_URL \
        --secret OS_USERNAME=$OS_USERNAME \
        --secret OS_PASSWORD=$OS_PASSWORD \
        --environment OS_USER_DOMAIN_NAME=$OS_USER_DOMAIN_NAME \
        --environment OS_PROJECT_NAME=$OS_PROJECT_NAME \
        --environment OS_PROJECT_DOMAIN_NAME=$OS_PROJECT_DOMAIN_NAME \
        --git-ref https://gitlab.com/fedora/bootc/tests/bootc-workflow-test \
        --git-url main \
        --compose $TF_COMPOSE \
        --arch $ARCH \
        --context "arch=$ARCH" \
        --context "distro=$CONTEXT_DISTRO" \
        --timeout 120
```

### Required environment variables

    TMT_PLAN                    TMT plans in tmt/plans folder
    TIER1_IMAGE_URL             bootc image to test
    TF_COMPOSE                  Testing farm runner installed OS compose
    CONTEXT_DISTRO              Distros used by TMT to filter test
    ARCH                        Test architecture
                                    "x86_64"
                                    "aarch64"
    QUAY_USERNAME               quay.io username
    QUAY_PASSWORD               quay.io password
    QUAY_SECRET                 Save into /etc/ostree/auth.json for authenticated registry
    DOWNLOAD_NODE               RHEL nightly compose download URL
    GCP_PROJECT                 Google Cloud Platform project name
    GCP_SERVICE_ACCOUNT_NAME    Google Cloud Platform service account name
    GCP_SERVICE_ACCOUNT_FILE_B64    Base64 encode string of Google Cloud Platform service account file
    AWS_ACCESS_KEY_ID           AWS access key id
    AWS_SECRET_ACCESS_KEY       AWS secrety key
    AWS_REGION                  AWS region
                                    "us-west-2"
    OS_USERNAME                 OpenStack username
    OS_PASSWORD                 OpenStack password
    OS_PROJECT_NAME             OpenStack project name
    OS_AUTH_URL                 OpenStack authentication URL
    OS_USER_DOMAIN_NAME         OpenStack domain name
    OS_PROJECT_DOMAIN_NAME      OpenStack project domain name
    GOVC_URL                    VMWare VSpehre server URL
    GOVC_USERNAME               VSPhere server username
    GOVC_PASSWORD               VSPhere server password
    GOVC_INSECURE               1
    AZURE_CLIENT_ID             Azure client ID
    AZURE_SECRET                Azure secret
    AZURE_TENANT                Azure tenant
    BEAKER_CLIENT_B64           Base64 encode string of Beaker client configure file
    BEAKER_KEYTAB_B64           Base64 encode string of kerberos keytab file
    KRB5_CONF_B64               Base64 encode string of kerberos krb5.conf file
    RHC_AK                      RHC active key
    RHC_ORGID                   RHC organization ID

### How to manage secrets used in test

Secrets represent sensitive information CI job needs to complete work. This sensitive information can be items like AWS access key, GCP service account, Azure secret, Quay.io username and password, VSPhere username and password, OpenStack username and password, etc.

GitLab provides support for [Google Cloud Secret Manager](https://docs.gitlab.com/ee/ci/secrets/gcp_secret_manager.html) secret management provider. All secrets used in this test are stored in [Google Cloud Secret Manager](https://docs.gitlab.com/ee/ci/secrets/gcp_secret_manager.html). Then you can use secrets stored in [Google Cloud Secret Manager](https://docs.gitlab.com/ee/ci/secrets/gcp_secret_manager.html) in CI jobs by [defining](https://docs.gitlab.com/ee/ci/secrets/gcp_secret_manager.html#configure-gitlab-cicd-to-use-gcp-secret-manager-secrets) them with the `gcp_secret_manager` keyword.

### Merge request gating

MR gating for this repository is to test `test code`. Test [commit lint](https://www.conventionalcommits.org/en/v1.0.0/), [spell check](https://github.com/codespell-project/codespell), [Shellcheck](https://www.shellcheck.net/), and [Yaml lint](https://yamllint.readthedocs.io/en/stable/) will be triggered automatically for each MR. The bootc workflow tests need to be triggered manually. Triggering bootc workflow test depends on the code change in MR. For example, if the MR just changes bootc install test code, only bootc install test should be triggered.

---
Made with ♥ by contributors!
