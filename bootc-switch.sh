#!/bin/bash
set -exuo pipefail

source tools/shared_lib.sh
dump_runner

ARCH=$(uname -m)

# SSH configurations
SSH_OPTIONS=(-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ConnectTimeout=5)
SSH_KEY=${TEMPDIR}/id_rsa
ssh-keygen -f "${SSH_KEY}" -N "" -q -t rsa-sha2-256 -b 2048
SSH_KEY_PUB="${SSH_KEY}.pub"
SSH_KEY_PUB_CONTENT=$(cat "$SSH_KEY_PUB")

BIB_IMAGE_URL="quay.io/centos-bootc/bootc-image-builder:latest"

# Upgrade scenario
case "$SCENARIO" in
    "9z-to-9y")
        DAY1_IMAGE_URL="registry.stage.redhat.io/rhel9/rhel-bootc:9.5"
        DAY2_IMAGE_URL="registry.stage.redhat.io/rhel9/rhel-bootc:9.6"
        ;;
    "9y-to-10y")
        DAY1_IMAGE_URL="registry.stage.redhat.io/rhel9/rhel-bootc:9.6"
        DAY2_IMAGE_URL="registry.stage.redhat.io/rhel10/rhel-bootc:10.0"
        ;;
    *)
        exit 1
        ;;
esac

greenprint "Login registry.stage.redhat.io"
sudo podman login -u "${REGISTRY_STAGE_USERNAME}" -p "${REGISTRY_STAGE_PASSWORD}" registry.stage.redhat.io

greenprint "Pull image $DAY1_IMAGE_URL"
sudo podman pull --quiet --tls-verify=false "$DAY1_IMAGE_URL"

greenprint "Configure bib config.toml"
tee -a "${TEMPDIR}/config.json" > /dev/null << EOF
{
  "customizations": {
    "user": [
      {
        "name": "root",
        "home": "/var/roothome",
        "key": "$SSH_KEY_PUB_CONTENT"
      }
    ]
  }
}
EOF

ROOTFS_LIST=( \
    "ext4" \
    "xfs" \
)
RND_LINE=$((RANDOM % 2))
ROOTFS="${ROOTFS_LIST[$RND_LINE]}"

sudo podman run \
    --rm \
    -it \
    --privileged \
    --pull=newer \
    --tls-verify=false \
    --security-opt label=type:unconfined_t \
    -v "${TEMPDIR}/config.json":/config.json:ro \
    -v "$TEMPDIR":/output \
    -v /var/lib/containers/storage:/var/lib/containers/storage \
    "$BIB_IMAGE_URL" \
    --type raw \
    --tls-verify=false \
    --chown "$(id -u "$(whoami)"):$(id -g "$(whoami)")" \
    --rootfs "$ROOTFS" \
    --local \
    "$DAY1_IMAGE_URL"

case "$ARCH" in
    "aarch64")
        sudo qemu-system-aarch64 \
            -name bootc-vm \
            -enable-kvm \
            -machine virt \
            -cpu host \
            -m 2G \
            -bios /usr/share/AAVMF/AAVMF_CODE.fd \
            -drive file="${TEMPDIR}/image/disk.raw",if=virtio,format=raw \
            -net nic,model=virtio \
            -net user,hostfwd=tcp::2222-:22 \
            -display none \
            -daemonize
        ;;
    "x86_64")
        sudo qemu-system-x86_64 \
            -name bootc-vm \
            -enable-kvm \
            -cpu host \
            -m 2G \
            -drive file="${TEMPDIR}/image/disk.raw",if=virtio,format=raw \
            -net nic,model=virtio \
            -net user,hostfwd=tcp::2222-:22 \
            -display none \
            -daemonize
        ;;
    *)
        redprint "Only support x86_64 and aarch64"
        exit 1
        ;;
esac

wait_for_ssh_up () {
    SSH_STATUS=$(ssh "${SSH_OPTIONS[@]}" -i "${TEMPDIR}/id_rsa" -p 2222 root@"${1}" '/bin/bash -c "echo -n READY"')
    if [[ $SSH_STATUS == READY ]]; then
        echo 1
    else
        echo 0
    fi
}

for _ in $(seq 0 30); do
    RESULT=$(wait_for_ssh_up "localhost")
    if [[ $RESULT == 1 ]]; then
        echo "SSH is ready now! 🥳"
        break
    fi
    sleep 10
done

ssh "${SSH_OPTIONS[@]}" -i "${TEMPDIR}/id_rsa" -p 2222 root@localhost "bootc status"

sed "s/REPLACE_ME/${REGISTRY_STAGE_SECRET}/; s/quay.io/registry.stage.redhat.io/" files/auth.template | tee "${TEMPDIR}"/auth.json > /dev/null
scp "${SSH_OPTIONS[@]}" -i "${TEMPDIR}/id_rsa" -P 2222 "${TEMPDIR}/auth.json" root@localhost:/etc/ostree/auth.json

ssh "${SSH_OPTIONS[@]}" -i "${TEMPDIR}/id_rsa" -p 2222 root@localhost "bootc switch --quiet $DAY2_IMAGE_URL"
ssh "${SSH_OPTIONS[@]}" -i "${TEMPDIR}/id_rsa" -p 2222 root@localhost "nohup systemctl reboot &>/dev/null & exit"
sleep 10

for _ in $(seq 0 30); do
    RESULT=$(wait_for_ssh_up "localhost")
    if [[ $RESULT == 1 ]]; then
        echo "SSH is ready now! 🥳"
        break
    fi
    sleep 10
done

ssh "${SSH_OPTIONS[@]}" -i "${TEMPDIR}/id_rsa" -p 2222 root@localhost "bootc status"

greenprint "🎉 All tests passed."
exit 0
